"""
Challenge Hackropole
https://hackropole.fr/fr/challenges/crypto/fcsc2022-crypto-a-laise/
"""

import string


text = """Gqfltwj emgj clgfv ! Aqltj rjqhjsksg ekxuaqs, ua xtwk
n'feuguvwb gkwp xwj, ujts f'npxkqvjgw nw tjuwcz
ugwygjtfkf qz uw efezg sqk gspwonu. Jgsfwb-aqmu f
Pspygk nj 29 cntnn hqzt dg igtwy fw xtvjg rkkunqf.
"""


challenge_key = "FCSC"


def encode(letter: string, key: int):
    if letter not in string.ascii_letters:
        return letter
    if 'A' <= letter <= 'Z':   #majuscule
        first_letter_code = ord('A')
    else:
        first_letter_code = ord('a')

    encode_int = first_letter_code + (ord(letter)-first_letter_code + key) % 26
    return chr(encode_int)


def decode (letter: string, key: int):
    return encode(letter, key*-1)


res = ""
i = 0
for l in text:
    if l not in string.ascii_letters:
        res += l
    else:
        key = ord(challenge_key[i%4]) - ord('A')
        res += decode(l, key)
        i += 1

print(res)
