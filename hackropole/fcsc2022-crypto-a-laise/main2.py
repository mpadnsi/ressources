cle = "FCSC"
n_cle = len(cle)

source = """Gqfltwj emgj clgfv ! Aqltj rjqhjsksg ekxuaqs, ua xtwk
n'feuguvwb gkwp xwj, ujts f'npxkqvjgw nw tjuwcz
ugwygjtfkf qz uw efezg sqk gspwonu. Jgsfwb-aqmu f
Pspygk nj 29 cntnn hqzt dg igtwy fw xtvjg rkkunqf."""
n_source = len(source)

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
decode = {alphabet[i]:(i%26) for i in range(len(alphabet))}


i = 0
k = 0
while i < n_source:
    lettre = source[i]
    if lettre in " !,'0123456789.\n-":
        lettre_decode = lettre
    else:
        lettre_decode = alphabet[ (decode[lettre] - decode[cle[k % n_cle]]) % 26 ]
        k += 1
    print(lettre_decode, end="")
    i = i + 1