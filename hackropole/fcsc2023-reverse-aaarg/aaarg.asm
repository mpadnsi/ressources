
aaarg:     format de fichier elf64-x86-64


Déassemblage de la section .init :

0000000000401000 <.init>:
  401000:	48 83 ec 08          	sub    $0x8,%rsp
  401004:	48 8b 05 ed 2f 00 00 	mov    0x2fed(%rip),%rax        # 403ff8 <strtoul@plt+0x2fb8>
  40100b:	48 85 c0             	test   %rax,%rax
  40100e:	74 02                	je     401012 <putc@plt-0x1e>
  401010:	ff d0                	call   *%rax
  401012:	48 83 c4 08          	add    $0x8,%rsp
  401016:	c3                   	ret

Déassemblage de la section .plt :

0000000000401020 <putc@plt-0x10>:
  401020:	ff 35 e2 2f 00 00    	push   0x2fe2(%rip)        # 404008 <strtoul@plt+0x2fc8>
  401026:	ff 25 e4 2f 00 00    	jmp    *0x2fe4(%rip)        # 404010 <strtoul@plt+0x2fd0>
  40102c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000401030 <putc@plt>:
  401030:	ff 25 e2 2f 00 00    	jmp    *0x2fe2(%rip)        # 404018 <strtoul@plt+0x2fd8>
  401036:	68 00 00 00 00       	push   $0x0
  40103b:	e9 e0 ff ff ff       	jmp    401020 <putc@plt-0x10>

0000000000401040 <strtoul@plt>:
  401040:	ff 25 da 2f 00 00    	jmp    *0x2fda(%rip)        # 404020 <strtoul@plt+0x2fe0>
  401046:	68 01 00 00 00       	push   $0x1
  40104b:	e9 d0 ff ff ff       	jmp    401020 <putc@plt-0x10>

Déassemblage de la section .text :

0000000000401050 <.text>:
  401050:	31 ed                	xor    %ebp,%ebp
  401052:	49 89 d1             	mov    %rdx,%r9
  401055:	5e                   	pop    %rsi
  401056:	48 89 e2             	mov    %rsp,%rdx
  401059:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
  40105d:	50                   	push   %rax
  40105e:	54                   	push   %rsp
  40105f:	49 c7 c0 80 12 40 00 	mov    $0x401280,%r8
  401066:	48 c7 c1 20 12 40 00 	mov    $0x401220,%rcx
  40106d:	48 c7 c7 90 11 40 00 	mov    $0x401190,%rdi
  401074:	ff 15 76 2f 00 00    	call   *0x2f76(%rip)        # 403ff0 <strtoul@plt+0x2fb0>
  40107a:	f4                   	hlt
  40107b:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)
  401080:	c3                   	ret
  401081:	66 2e 0f 1f 84 00 00 	cs nopw 0x0(%rax,%rax,1)
  401088:	00 00 00 
  40108b:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)
  401090:	b8 38 40 40 00       	mov    $0x404038,%eax
  401095:	48 3d 38 40 40 00    	cmp    $0x404038,%rax
  40109b:	74 13                	je     4010b0 <strtoul@plt+0x70>
  40109d:	b8 00 00 00 00       	mov    $0x0,%eax
  4010a2:	48 85 c0             	test   %rax,%rax
  4010a5:	74 09                	je     4010b0 <strtoul@plt+0x70>
  4010a7:	bf 38 40 40 00       	mov    $0x404038,%edi
  4010ac:	ff e0                	jmp    *%rax
  4010ae:	66 90                	xchg   %ax,%ax
  4010b0:	c3                   	ret
  4010b1:	66 66 2e 0f 1f 84 00 	data16 cs nopw 0x0(%rax,%rax,1)
  4010b8:	00 00 00 00 
  4010bc:	0f 1f 40 00          	nopl   0x0(%rax)
  4010c0:	be 38 40 40 00       	mov    $0x404038,%esi
  4010c5:	48 81 ee 38 40 40 00 	sub    $0x404038,%rsi
  4010cc:	48 c1 fe 03          	sar    $0x3,%rsi
  4010d0:	48 89 f0             	mov    %rsi,%rax
  4010d3:	48 c1 e8 3f          	shr    $0x3f,%rax
  4010d7:	48 01 c6             	add    %rax,%rsi
  4010da:	48 d1 fe             	sar    %rsi
  4010dd:	74 11                	je     4010f0 <strtoul@plt+0xb0>
  4010df:	b8 00 00 00 00       	mov    $0x0,%eax
  4010e4:	48 85 c0             	test   %rax,%rax
  4010e7:	74 07                	je     4010f0 <strtoul@plt+0xb0>
  4010e9:	bf 38 40 40 00       	mov    $0x404038,%edi
  4010ee:	ff e0                	jmp    *%rax
  4010f0:	c3                   	ret
  4010f1:	66 66 2e 0f 1f 84 00 	data16 cs nopw 0x0(%rax,%rax,1)
  4010f8:	00 00 00 00 
  4010fc:	0f 1f 40 00          	nopl   0x0(%rax)
  401100:	80 3d 39 2f 00 00 00 	cmpb   $0x0,0x2f39(%rip)        # 404040 <stdout@GLIBC_2.2.5+0x8>
  401107:	75 17                	jne    401120 <strtoul@plt+0xe0>
  401109:	55                   	push   %rbp
  40110a:	48 89 e5             	mov    %rsp,%rbp
  40110d:	e8 7e ff ff ff       	call   401090 <strtoul@plt+0x50>
  401112:	c6 05 27 2f 00 00 01 	movb   $0x1,0x2f27(%rip)        # 404040 <stdout@GLIBC_2.2.5+0x8>
  401119:	5d                   	pop    %rbp
  40111a:	c3                   	ret
  40111b:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)
  401120:	c3                   	ret
  401121:	66 66 2e 0f 1f 84 00 	data16 cs nopw 0x0(%rax,%rax,1)
  401128:	00 00 00 00 
  40112c:	0f 1f 40 00          	nopl   0x0(%rax)
  401130:	eb 8e                	jmp    4010c0 <strtoul@plt+0x80>
  401132:	66 2e 0f 1f 84 00 00 	cs nopw 0x0(%rax,%rax,1)
  401139:	00 00 00 
  40113c:	0f 1f 40 00          	nopl   0x0(%rax)
  401140:	53                   	push   %rbx
  401141:	31 db                	xor    %ebx,%ebx
  401143:	66 2e 0f 1f 84 00 00 	cs nopw 0x0(%rax,%rax,1)
  40114a:	00 00 00 
  40114d:	0f 1f 00             	nopl   (%rax)
  401150:	0f be bb 10 20 40 00 	movsbl 0x402010(%rbx),%edi
  401157:	48 8b 35 da 2e 00 00 	mov    0x2eda(%rip),%rsi        # 404038 <stdout@GLIBC_2.2.5>
  40115e:	e8 cd fe ff ff       	call   401030 <putc@plt>
  401163:	48 83 c3 04          	add    $0x4,%rbx
  401167:	48 81 fb 16 01 00 00 	cmp    $0x116,%rbx
  40116e:	72 e0                	jb     401150 <strtoul@plt+0x110>
  401170:	48 8b 35 c1 2e 00 00 	mov    0x2ec1(%rip),%rsi        # 404038 <stdout@GLIBC_2.2.5>
  401177:	bf 0a 00 00 00       	mov    $0xa,%edi
  40117c:	5b                   	pop    %rbx
  40117d:	e9 ae fe ff ff       	jmp    401030 <putc@plt>
  401182:	66 2e 0f 1f 84 00 00 	cs nopw 0x0(%rax,%rax,1)
  401189:	00 00 00 
  40118c:	0f 1f 40 00          	nopl   0x0(%rax)
  401190:	b8 01 00 00 00       	mov    $0x1,%eax
  401195:	83 ff 02             	cmp    $0x2,%edi
  401198:	7c 7e                	jl     401218 <strtoul@plt+0x1d8>
  40119a:	53                   	push   %rbx
  40119b:	48 83 ec 10          	sub    $0x10,%rsp
  40119f:	89 fb                	mov    %edi,%ebx
  4011a1:	48 8b 7e 08          	mov    0x8(%rsi),%rdi
  4011a5:	48 8d 74 24 08       	lea    0x8(%rsp),%rsi
  4011aa:	ba 0a 00 00 00       	mov    $0xa,%edx
  4011af:	e8 8c fe ff ff       	call   401040 <strtoul@plt>
  4011b4:	48 89 c1             	mov    %rax,%rcx
  4011b7:	48 8b 54 24 08       	mov    0x8(%rsp),%rdx
  4011bc:	b8 01 00 00 00       	mov    $0x1,%eax
  4011c1:	80 3a 00             	cmpb   $0x0,(%rdx)
  4011c4:	75 4d                	jne    401213 <strtoul@plt+0x1d3>
  4011c6:	f7 db                	neg    %ebx
  4011c8:	48 63 d3             	movslq %ebx,%rdx
  4011cb:	b8 02 00 00 00       	mov    $0x2,%eax
  4011d0:	48 39 d1             	cmp    %rdx,%rcx
  4011d3:	75 3e                	jne    401213 <strtoul@plt+0x1d3>
  4011d5:	31 db                	xor    %ebx,%ebx
  4011d7:	66 0f 1f 84 00 00 00 	nopw   0x0(%rax,%rax,1)
  4011de:	00 00 
  4011e0:	0f be bb 10 20 40 00 	movsbl 0x402010(%rbx),%edi
  4011e7:	48 8b 35 4a 2e 00 00 	mov    0x2e4a(%rip),%rsi        # 404038 <stdout@GLIBC_2.2.5>
  4011ee:	e8 3d fe ff ff       	call   401030 <putc@plt>
  4011f3:	48 83 c3 04          	add    $0x4,%rbx
  4011f7:	48 81 fb 16 01 00 00 	cmp    $0x116,%rbx
  4011fe:	72 e0                	jb     4011e0 <strtoul@plt+0x1a0>
  401200:	48 8b 35 31 2e 00 00 	mov    0x2e31(%rip),%rsi        # 404038 <stdout@GLIBC_2.2.5>
  401207:	bf 0a 00 00 00       	mov    $0xa,%edi
  40120c:	e8 1f fe ff ff       	call   401030 <putc@plt>
  401211:	31 c0                	xor    %eax,%eax
  401213:	48 83 c4 10          	add    $0x10,%rsp
  401217:	5b                   	pop    %rbx
  401218:	c3                   	ret
  401219:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
  401220:	41 57                	push   %r15
  401222:	49 89 d7             	mov    %rdx,%r15
  401225:	41 56                	push   %r14
  401227:	49 89 f6             	mov    %rsi,%r14
  40122a:	41 55                	push   %r13
  40122c:	41 89 fd             	mov    %edi,%r13d
  40122f:	41 54                	push   %r12
  401231:	4c 8d 25 c8 2b 00 00 	lea    0x2bc8(%rip),%r12        # 403e00 <strtoul@plt+0x2dc0>
  401238:	55                   	push   %rbp
  401239:	48 8d 2d c8 2b 00 00 	lea    0x2bc8(%rip),%rbp        # 403e08 <strtoul@plt+0x2dc8>
  401240:	53                   	push   %rbx
  401241:	4c 29 e5             	sub    %r12,%rbp
  401244:	48 83 ec 08          	sub    $0x8,%rsp
  401248:	e8 b3 fd ff ff       	call   401000 <putc@plt-0x30>
  40124d:	48 c1 fd 03          	sar    $0x3,%rbp
  401251:	74 1b                	je     40126e <strtoul@plt+0x22e>
  401253:	31 db                	xor    %ebx,%ebx
  401255:	0f 1f 00             	nopl   (%rax)
  401258:	4c 89 fa             	mov    %r15,%rdx
  40125b:	4c 89 f6             	mov    %r14,%rsi
  40125e:	44 89 ef             	mov    %r13d,%edi
  401261:	41 ff 14 dc          	call   *(%r12,%rbx,8)
  401265:	48 83 c3 01          	add    $0x1,%rbx
  401269:	48 39 dd             	cmp    %rbx,%rbp
  40126c:	75 ea                	jne    401258 <strtoul@plt+0x218>
  40126e:	48 83 c4 08          	add    $0x8,%rsp
  401272:	5b                   	pop    %rbx
  401273:	5d                   	pop    %rbp
  401274:	41 5c                	pop    %r12
  401276:	41 5d                	pop    %r13
  401278:	41 5e                	pop    %r14
  40127a:	41 5f                	pop    %r15
  40127c:	c3                   	ret
  40127d:	0f 1f 00             	nopl   (%rax)
  401280:	c3                   	ret

Déassemblage de la section .fini :

0000000000401284 <.fini>:
  401284:	48 83 ec 08          	sub    $0x8,%rsp
  401288:	48 83 c4 08          	add    $0x8,%rsp
  40128c:	c3                   	ret
