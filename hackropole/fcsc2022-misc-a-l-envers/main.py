from time import sleep
import socket

# connexion TCP/IP
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

sock.connect(('localhost', 4000))

fin = False
while not fin:
    # pause 50 ms pour ne pas perdre des caractères
    sleep(0.050)
    # réception des données
    data = sock.recv(1024)
    print(f"Reçu : {data}")
    # diviser en lignes
    lines = data.decode().split('\n')
    
    for line in lines :
        if "Congratulations" in line:
            fin = True
        elif len(line) > 0 and line[0] == '>':    # ligne à traiter
            # supprimer les '>>> '
            line = line[4:]
            # inverser la ligne
            rep = line[-1::-1]
            # ajouter le ENTER en fin de ligne
            print(f"Envoie : {rep}")
            rep += '\n'
            # renvoyer la réponse au serveur
            sock.sendall(rep.encode())

sock.close()
